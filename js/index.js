import Handlebars from 'handlebars';
import { TweenMax, TimelineMax } from 'gsap';

document.addEventListener("DOMContentLoaded", function(event) {
	const cardContainer = document.querySelector('.js-memory')

	const Memory = function(container){

		this.setup = function(){
		    this.shuffle = function(array) {
			  array.sort(() => Math.random() - 0.5);
			}

			this.popup = document.querySelector('.popup')
			this.animating = false
			this.points = 0
			this.numberCouples = container.getAttribute("data-cards") || 4
			this.total = this.numberCouples * 2			
			this.values = []
			this.source = document.getElementById("entry-template").innerHTML;
			this.template = Handlebars.compile(this.source);
			this.els = []
			this.el = document.querySelector('.container')
			this.timerContainer = document.querySelector('.js-timer')

			for(let i = 0; i < this.numberCouples; i++) {
				this.image = "https://picsum.photos/id/" + i * 22 + "/600/600"
				this.context = {title: i + 1, body: "This is my first post!", image: this.image};
				this.html    = this.template(this.context);
				this.newDiv = document.createElement("div"); 
				this.newDiv.value = i + 1
				this.newDiv.classList.add("entry_outer")
				this.newDiv.innerHTML = this.html
				this.clone = this.newDiv.cloneNode(true)
				this.clone.value = i + 1
				this.els.push(this.newDiv)
				this.els.push(this.clone)
			}
			this.shuffle(this.els)
			TweenMax.set(this.popup,{
				opacity: 0
			})
			this.els.forEach((e) => {
				TweenMax.set(e,{
					opacity: 0,
					y: 10
				})
				this.el.appendChild(e)

				TweenMax.staggerTo(this.els, .4, {
					opacity:1,
					y:0,
					ease:Power4.easeOut
				}, .1);
				e.addEventListener("click",() => {	
					this.checkValueAndRotate(e)
				})
			})

			this.seconds = 0
			this.time = setInterval(this.countSeconds.bind(this), 1000);
		}
		this.countSeconds = function(){
			this.seconds ++;
			this.timerContainer.innerHTML = this.seconds;
		}


		this.checkValueAndRotate = function(e) {
			if ( this.animating ) {
				return false
			}
			const divRotate = e.querySelector('.entry_inner')
			if (this.values[0] == e){
				this.rotateAnim(divRotate,0,this.stopAnimation.bind(this));
				this.values = []
				return false
			}
			this.values.push(e)
			this.rotateAnim(divRotate,-180,this.compareValues.bind(this));
		}

		this.compareValues = function() {

			this.animating = false
			if ( this.values.length == 2 ){
				if (this.values[0].value == this.values[1].value){
					this.points += 2
					this.cardDisabled(this.values);
				} else {
					this.cardReset(this.values);
				}
			this.values = []
			}	
		}

		this.cardDisabled = function(els){
			this.animating = true
			TweenMax.to(els,.4,{
				opacity:0,
				className:"+=disabled",
				onComplete: () => {
					this.animating = false
					if ( this.points == this.total ) {
						this.end()
					}
				}
			})
		}

		this.stopAnimation = function() {
			this.animating = false
		}

		this.cardReset = function(els){
			els.forEach((el) => {
				const t = el.querySelector('.entry_inner')
				this.rotateAnim(t,0,this.stopAnimation.bind(this));
			})
		}

		this.rotateAnim = function(el,deg,callback) {
			this.animating = true
			TweenMax.to(el, .4, {
				rotationY: deg + "deg",
				onComplete: callback
			})

		}
		this.end = function(){
			clearInterval(this.time)
			this.endAnimation()
			TweenMax.to(this.popup,1,{
				opacity: 1
			})
		}

		this.endAnimation = function() {
			this.els.forEach((el) => {
				el.classList.remove("disabled")
			})
			TweenMax.staggerTo(this.els, .4, {
					opacity:1,
					ease:Power4.easeOut
			}, .1);
			this.els.forEach((el) => {
				const t = el.querySelector('.entry_inner')
				TweenMax.to(t, 2, {
				rotationY: "+180",
				ease: Linear.easeNone,
				repeat: -1
				})
			})

		}
	}	

	const memoryApp = new Memory(cardContainer)
	memoryApp.setup();
});